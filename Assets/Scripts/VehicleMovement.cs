﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{


    public float movementSpeed = 10f; // this value represents how fast the object can move
    public float turningSpeed = 60f; // this value represents how fast the object turns from side to side



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime; // this line gets input from keys to on the horizontal axis in the inspector
        transform.Rotate(0, horizontal, 0); // this line makes the tank turn on the y axis in response to the earlier input

        float vertical = Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime; // this line gets input from keys to on the vertical axis in the inspector
        transform.Translate(0, 0, vertical); // this line makes the tank turn on the z axis in response to the earlier input


    }
}
