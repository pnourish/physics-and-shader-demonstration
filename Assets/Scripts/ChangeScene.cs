﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public int sceneChoice;
    public bool changeOnSpace = false;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (changeOnSpace == true)
        {
            if (Input.GetKeyDown("space"))
            {
                Application.LoadLevel(sceneChoice);

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Application.LoadLevel(sceneChoice);
        }

    }


}
