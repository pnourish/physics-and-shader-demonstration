﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RagdollTrigger : MonoBehaviour
{

    public Ragdoll ragdoll1;
    public Rigidbody ragdollHead;
    float impulsePower = 70;

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            if (ragdoll1 != null)
            {
                    ragdoll1.RagdollOn = !ragdoll1.RagdollOn;
                    StartCoroutine(ApplyForceToHead(2.0f, impulsePower));
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Ragdoll ragdoll = other.gameObject.GetComponentInParent<Ragdoll>();
        if (ragdoll != null) ragdoll.RagdollOn = true;
    }

    private IEnumerator ApplyForceToHead(float time, float force)
    {
        float timer = 0.0f;
        while(timer < time)
        {
            timer += Time.deltaTime;
            ragdollHead.AddForce(Vector3.forward * force, ForceMode.Force);
            yield return new WaitForEndOfFrame();
        }
    }
}


//Have it that every time the head collides with anything a particle effect similar to blood appears at the location of the head with varying size.