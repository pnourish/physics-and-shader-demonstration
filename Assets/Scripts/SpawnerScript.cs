﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpawnerScript : MonoBehaviour
{
    public Transform prefab;
    private float timer = 0;
    // Use this for initialization 
    void Start ()
    {

    }

    // Update is called once per frame 
    void Update ()
    {
        if (Input.anyKey == true )
        {
            Instantiate(prefab, transform.position, Quaternion.identity);
            timer = 0;
        }
        timer += Time.deltaTime;
    }
}