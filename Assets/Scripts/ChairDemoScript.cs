﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ChairDemoScript : MyEvent
{
    public float forceOnFire = 5000;
    public  bool fire = false;
    bool canFire = true;
    Rigidbody rigidbody = null;
    // Use this for initialization 
    void Start()
    {

    }
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
    }
    // Update is called once per frame

    public override void TriggerEvent()
    {
        rigidbody.isKinematic = false;
        rigidbody.AddForce(transform.forward * forceOnFire);
        canFire = false;
        base.TriggerEvent();
    }
}