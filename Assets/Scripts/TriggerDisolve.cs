﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TriggerDisolve : MonoBehaviour {

    GameObject player;
    Renderer shaderRenderer;
    float dissolveAmount;
    float distanceFromPlayerToObject;
    public float distanceToBeginShowing;
    public float distanceForCompletelyGone;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        shaderRenderer = GetComponent<Renderer>(); 
	}
	
	// Update is called once per frame
	void Update ()
    {
        distanceFromPlayerToObject = Vector3.Magnitude(this.gameObject.transform.position - player.transform.position);


        if (distanceFromPlayerToObject < distanceToBeginShowing)
        {
            float t = (distanceFromPlayerToObject - distanceForCompletelyGone) / (distanceToBeginShowing - distanceForCompletelyGone);
            t = 1 - Mathf.Clamp01(t);
            BeginDisolving(t);
        }
        else
        {
            shaderRenderer.sharedMaterial.SetFloat("_Dissolveamount", 0);
        }
	}


    void BeginDisolving(float a_maxDistance)
    {
        shaderRenderer.sharedMaterial.SetFloat("_Dissolveamount", a_maxDistance);
    }
}
