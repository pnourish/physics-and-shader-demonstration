﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodMarkSpawner : MonoBehaviour
{




    public GameObject bloodSplatterToSpawn;
    bool bloodSpawned = false;
    float resetTimer = 0.0f;
    float spawnAgainTime = 0.10f;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (bloodSpawned == true)
        {
            resetTimer += Time.deltaTime;
            if (resetTimer >= spawnAgainTime)
            {
                resetTimer = 0;
                bloodSpawned = false;
            }
        }
    }


    void OnParticleCollision(GameObject other)
    {
        if (bloodSpawned == false)
        {
            ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
            ParticleCollisionEvent[] collisions = new ParticleCollisionEvent[particleSystem.GetSafeCollisionEventSize()];
            int numberOfCollisions = particleSystem.GetCollisionEvents(this.gameObject, collisions);

            for (int i = 0; i < numberOfCollisions; i++)
            {

                    Vector3 spawnPos = new Vector3(collisions[i].intersection.x, collisions[i].intersection.y, collisions[i].intersection.z);
                    Quaternion spawnRot = Quaternion.LookRotation(-collisions[i].normal);

                    spawnPos -= 0.05f * collisions[i].normal;

                    //spawnPos.x = spawnPos.x * 0.99f;
                    //spawnPos.y = spawnPos.y * 0.99f;
                    //spawnPos.z = spawnPos.z * 0.99f;

                Instantiate(bloodSplatterToSpawn, spawnPos, spawnRot);
                    bloodSpawned = true;    
            }
        }
    }
}
