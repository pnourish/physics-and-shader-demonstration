﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfKillScript : MonoBehaviour {

    float timeAlive;
    public float killTime;

	// Use this for initialization
	void Start ()
    {
        timeAlive = 0;
        killTime = 5;	
	}
	
	// Update is called once per frame
	void Update ()
    {
        timeAlive += Time.deltaTime;
        if (timeAlive >= killTime)
            GameObject.Destroy(this.gameObject);	
	}
}
