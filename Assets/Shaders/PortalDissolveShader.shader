// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33410,y:32719,varname:node_4013,prsc:2|emission-4672-RGB,clip-7747-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4549,x:32924,y:32814,ptovrint:False,ptlb:Ramp,ptin:_Ramp,varname:node_4549,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:943e61fc51848304685d6ea8de81ba23,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Append,id:6876,x:33000,y:32614,varname:node_6876,prsc:2|A-3024-OUT,B-7675-OUT;n:type:ShaderForge.SFN_Vector1,id:7675,x:32914,y:32742,varname:node_7675,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2d,id:4672,x:33174,y:32703,varname:node_4672,prsc:2,tex:943e61fc51848304685d6ea8de81ba23,ntxv:0,isnm:False|UVIN-6876-OUT,TEX-4549-TEX;n:type:ShaderForge.SFN_Tex2d,id:1561,x:31905,y:33001,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_1561,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:830,x:31630,y:32741,ptovrint:False,ptlb:Dissolve amount,ptin:_Dissolveamount,varname:node_830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:7747,x:32324,y:32837,varname:node_7747,prsc:2|A-3803-OUT,B-1561-R;n:type:ShaderForge.SFN_RemapRange,id:3803,x:32129,y:32742,varname:node_3803,prsc:2,frmn:0,frmx:1,tomn:-0.6,tomx:0.6|IN-7562-OUT;n:type:ShaderForge.SFN_OneMinus,id:7562,x:31965,y:32742,varname:node_7562,prsc:2|IN-830-OUT;n:type:ShaderForge.SFN_RemapRange,id:156,x:32481,y:32671,varname:node_156,prsc:2,frmn:0,frmx:1,tomn:-4,tomx:4|IN-7747-OUT;n:type:ShaderForge.SFN_OneMinus,id:3024,x:32814,y:32671,varname:node_3024,prsc:2|IN-7278-OUT;n:type:ShaderForge.SFN_Clamp01,id:7278,x:32643,y:32671,varname:node_7278,prsc:2|IN-156-OUT;proporder:4549-1561-830;pass:END;sub:END;*/

Shader "Shader Forge/PortalDissolveShader" {
    Properties {
        _Ramp ("Ramp", 2D) = "black" {}
        _Noise ("Noise", 2D) = "white" {}
        _Dissolveamount ("Dissolve amount", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Dissolveamount;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_7747 = (((1.0 - _Dissolveamount)*1.2+-0.6)+_Noise_var.r);
                clip(node_7747 - 0.5);
////// Lighting:
////// Emissive:
                float2 node_6876 = float2((1.0 - saturate((node_7747*8.0+-4.0))),0.0);
                float4 node_4672 = tex2D(_Ramp,TRANSFORM_TEX(node_6876, _Ramp));
                float3 emissive = node_4672.rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Dissolveamount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_7747 = (((1.0 - _Dissolveamount)*1.2+-0.6)+_Noise_var.r);
                clip(node_7747 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
