// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33983,y:33064,varname:node_4013,prsc:2|emission-1276-OUT,voffset-4070-OUT;n:type:ShaderForge.SFN_Color,id:7957,x:32916,y:32134,ptovrint:False,ptlb:Color_copy,ptin:_Color_copy,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6965513,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2943,x:32916,y:32320,ptovrint:False,ptlb:Colour_copy,ptin:_Colour_copy,varname:_Colour_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9172416,c3:1,c4:1;n:type:ShaderForge.SFN_ComponentMask,id:4657,x:32029,y:32653,varname:node_4657,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-3667-UVOUT;n:type:ShaderForge.SFN_Time,id:1601,x:32029,y:32825,varname:node_1601,prsc:2;n:type:ShaderForge.SFN_Lerp,id:6387,x:33363,y:32611,varname:node_6387,prsc:2|A-2943-RGB,B-7957-RGB,T-5344-OUT;n:type:ShaderForge.SFN_Vector1,id:2194,x:32221,y:32540,varname:node_2194,prsc:2,v1:-1;n:type:ShaderForge.SFN_Sin,id:5344,x:32916,y:32571,varname:node_5344,prsc:2|IN-4558-OUT;n:type:ShaderForge.SFN_Multiply,id:4558,x:32426,y:32631,varname:node_4558,prsc:2|A-2194-OUT,B-2940-OUT;n:type:ShaderForge.SFN_Add,id:2940,x:32254,y:32642,varname:node_2940,prsc:2|A-4657-OUT,B-1601-T;n:type:ShaderForge.SFN_NormalVector,id:8316,x:33031,y:32876,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:5001,x:33339,y:32734,varname:node_5001,prsc:2|A-5344-OUT,B-5210-OUT,C-8316-OUT;n:type:ShaderForge.SFN_Vector1,id:5210,x:33048,y:32747,varname:node_5210,prsc:2,v1:1;n:type:ShaderForge.SFN_TexCoord,id:3667,x:31790,y:32609,varname:node_3667,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:9774,x:33053,y:33187,ptovrint:False,ptlb:Color_copy_copy,ptin:_Color_copy_copy,varname:_Color_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6965513,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2182,x:33053,y:33352,ptovrint:False,ptlb:Colour_copy_copy,ptin:_Colour_copy_copy,varname:_Colour_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9172416,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:1276,x:33286,y:33403,varname:node_1276,prsc:2|A-2182-RGB,B-9774-RGB,T-3703-OUT;n:type:ShaderForge.SFN_TexCoord,id:7737,x:31646,y:33616,varname:node_7737,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ComponentMask,id:4620,x:31977,y:33486,varname:node_4620,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7737-V;n:type:ShaderForge.SFN_Clamp01,id:3703,x:33053,y:33542,varname:node_3703,prsc:2|IN-1440-OUT;n:type:ShaderForge.SFN_Sin,id:8985,x:32701,y:33548,varname:node_8985,prsc:2|IN-5103-OUT;n:type:ShaderForge.SFN_Multiply,id:5103,x:32530,y:33548,varname:node_5103,prsc:2|A-5391-OUT,B-8364-OUT,C-1857-OUT;n:type:ShaderForge.SFN_Vector1,id:5391,x:32337,y:33458,varname:node_5391,prsc:2,v1:5;n:type:ShaderForge.SFN_RemapRange,id:1440,x:32887,y:33548,varname:node_1440,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-8985-OUT;n:type:ShaderForge.SFN_Tau,id:1857,x:32404,y:33730,varname:node_1857,prsc:2;n:type:ShaderForge.SFN_Add,id:8364,x:32268,y:33566,varname:node_8364,prsc:2|A-4620-OUT,B-2216-TSL;n:type:ShaderForge.SFN_Time,id:2216,x:32039,y:33669,varname:node_2216,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:8561,x:33053,y:33697,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:4070,x:33314,y:33661,varname:node_4070,prsc:2|A-3703-OUT,B-8561-OUT,C-5813-OUT;n:type:ShaderForge.SFN_Vector1,id:5813,x:33063,y:33865,varname:node_5813,prsc:2,v1:0.3;n:type:ShaderForge.SFN_FragmentPosition,id:266,x:31748,y:33439,varname:node_266,prsc:2;proporder:7957-2943-9774-2182;pass:END;sub:END;*/

Shader "Shader Forge/PortalWaveShader" {
    Properties {
        _Color_copy ("Color_copy", Color) = (0.6965513,0,1,1)
        _Colour_copy ("Colour_copy", Color) = (0,0.9172416,1,1)
        _Color_copy_copy ("Color_copy_copy", Color) = (0.6965513,0,1,1)
        _Colour_copy_copy ("Colour_copy_copy", Color) = (0,0.9172416,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color_copy_copy;
            uniform float4 _Colour_copy_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_2216 = _Time + _TimeEditor;
                float node_3703 = saturate((sin((5.0*(o.uv0.g.r+node_2216.r)*6.28318530718))*0.5+0.5));
                v.vertex.xyz += (node_3703*v.normal*0.3);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_2216 = _Time + _TimeEditor;
                float node_3703 = saturate((sin((5.0*(i.uv0.g.r+node_2216.r)*6.28318530718))*0.5+0.5));
                float3 emissive = lerp(_Colour_copy_copy.rgb,_Color_copy_copy.rgb,node_3703);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_2216 = _Time + _TimeEditor;
                float node_3703 = saturate((sin((5.0*(o.uv0.g.r+node_2216.r)*6.28318530718))*0.5+0.5));
                v.vertex.xyz += (node_3703*v.normal*0.3);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
