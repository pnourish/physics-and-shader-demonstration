// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33471,y:32625,varname:node_4013,prsc:2|emission-3700-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32852,y:32680,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5172414,c3:0,c4:1;n:type:ShaderForge.SFN_TexCoord,id:897,x:32035,y:33084,varname:node_897,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:672,x:32852,y:32518,ptovrint:False,ptlb:Colour,ptin:_Colour,varname:node_672,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_ComponentMask,id:9168,x:32230,y:32954,varname:node_9168,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1533-Y;n:type:ShaderForge.SFN_Time,id:5635,x:32230,y:33126,varname:node_5635,prsc:2;n:type:ShaderForge.SFN_Lerp,id:3700,x:33210,y:32788,varname:node_3700,prsc:2|A-672-RGB,B-1304-RGB,T-8651-OUT;n:type:ShaderForge.SFN_Vector1,id:1634,x:32431,y:32833,varname:node_1634,prsc:2,v1:1;n:type:ShaderForge.SFN_Clamp01,id:8651,x:33030,y:32899,varname:node_8651,prsc:2|IN-7116-OUT;n:type:ShaderForge.SFN_Sin,id:7116,x:32852,y:32943,varname:node_7116,prsc:2|IN-6207-OUT;n:type:ShaderForge.SFN_Multiply,id:6207,x:32662,y:32943,varname:node_6207,prsc:2|A-1634-OUT,B-8792-OUT;n:type:ShaderForge.SFN_Add,id:8792,x:32455,y:32943,varname:node_8792,prsc:2|A-9168-OUT,B-5635-T;n:type:ShaderForge.SFN_FragmentPosition,id:1533,x:32035,y:32943,varname:node_1533,prsc:2;n:type:ShaderForge.SFN_Tau,id:4497,x:32511,y:33133,varname:node_4497,prsc:2;proporder:1304-672;pass:END;sub:END;*/

Shader "Shader Forge/LavaShader1" {
    Properties {
        _Color ("Color", Color) = (1,0.5172414,0,1)
        _Colour ("Colour", Color) = (1,0,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float4 _Colour;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_1634 = 1.0;
                float node_9168 = i.posWorld.g.r;
                float4 node_5635 = _Time + _TimeEditor;
                float3 emissive = lerp(_Colour.rgb,_Color.rgb,saturate(sin((node_1634*(node_9168+node_5635.g)))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
